N-Cache
=======

Cache and access NAStJA simulations.

# Installation

## With pip

```bash
$ pip install --upgrade "git+ssh://git@gitlab.com:nastja/cell_sim_management.git#subdirectory=client"
```

You might want to use [venv][venv].

## Development

Install the project as an [editable install][edit-install] to make easy changes.

- Clone project.
- Navigate to `client/` subdir.
- Execute the following:

```bash
$ pip install -e .
```

# Usage as executeable

After installation you have access to the `n-cache` executeable.
This is its usage prompt:

```
usage: n-cache [-h] [--nastja-cache-url NASTJA_CACHE_URL] {repo,cluster,runner} ...

Maintainer: David <kit@david-wilkening.de>

positional arguments:
  {repo,cluster,runner}

optional arguments:
  -h, --help         show this help message and exit
  --nastja-cache-url NASTJA_CACHE_URL
                     Base URL of global cache. Defaults to NASTJA_CACHE_URL=
```

The URL for the remote repository can be set as an environment variable.
It is default beahvaiour to create a sqlite db as local cache in the current working directory.
At the moment, N-Cache requires a `nastja` binary to be present in the current working directory.

Different functionalities are divided into subcommands.

## `repo`

Query simulations from local_db, local_simulation or remote and output to `out/` directory.

```
usage: n-cache repo [-h] [--db-conn DB_CONN] [--skip-download] [--skip-upload] [--skip-simulation] config overrides

positional arguments:
  config             Base config file
  overrides          Description of alterations to base config

optional arguments:
  -h, --help         show this help message and exit
  --db-conn DB_CONN  DB connection string. Defaults to "sqlite+pysqlite:///local_repo.db"
  --skip-download    Skip downloading simulations from global repository
  --skip-upload      Skip uploading simulations to global repository
  --skip-simulation  Do not execute NAStJA locally
```

## `cluster`

Send a job request to be executed on a remote system. The result will be stored in the remote repo
and can downloaded via `n-cache repo`.

```
usage: n-cache cluster [-h] [--db-conn DB_CONN] config overrides

positional arguments:
  config             Base config file
  overrides          Description of alterations to base config

optional arguments:
  -h, --help         show this help message and exit
  --db-conn DB_CONN  DB connection string. Defaults to "sqlite+pysqlite:///local_repo.db"
```

## `runner`

Simulate jobs from remote and uploads them. Uses 8 concurrent NAStJA instances locally.

```
usage: n-cache runner [-h]

optional arguments:
  -h, --help  show this help message and exit
```

# Usage as module

The `n_cache` module can also be imported into your own scripts.
Create an instance of `n_cache.Config` and use it to initialize `n_cache.NCache`.
Alternatively, you can use the `Config` instance to initialize `n_cache.NumpyConsume` and query simulations as a `numpy.ndarray`.

# Simulation specification

N-Cache uses 2 files to specify a batch of simulations.
The 'base file' is a normal NAStJA configuration (Pleas make sure it ouputs csv data frames.).
This file is then combined with a file describing overrides.
Such file might look like this:

```json
{
    "overrides": [
        {
            "path": "/Demo1",
            "intervall": [1,5],
            "step": 2
        },
        {
            "path": "/Demo2",
            "enum": ["Foo", "Bar"]
        }
    ]
}
```

The paths are specified as [json pointers][json-pointer].
N-Cache calculates the cross product of all overrides to generate all configs used for simulation.

[venv]: https://docs.python.org/3/library/venv.html
[edit-install]: https://pip.pypa.io/en/stable/cli/pip_install/#editable-installs
[json-pointer]: https://datatracker.ietf.org/doc/html/rfc6901