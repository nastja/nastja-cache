#!/bin/bash

wget https://gitlab.com/nastja/nastja/-/jobs/artifacts/master/raw/build_nvcc_clang_8/artifacts.tar.gz?job=build_nvcc_clang_8 -O nastja.tar.gz
mkdir nastja_build/
tar -zxf nastja.tar.gz --directory nastja_build/
rm nastja.tar.gz
mv nastja_build/nastja .
rm -rf nastja_build
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get -y install --no-install-recommends libomp5 openmpi-bin
rm -rf /var/lib/apt/lists/*