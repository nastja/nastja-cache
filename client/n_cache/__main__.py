import logging
import sys

from n_cache.cli.parser import run


def main() -> None:
    rootLogger = logging.getLogger()

    rootLogger.setLevel(logging.INFO)

    stdoutHandler = logging.StreamHandler(stream=sys.stdout)
    stdoutHandler.addFilter(lambda x: x.levelno == logging.INFO)

    stderrHandler = logging.StreamHandler(stream=sys.stdout)
    stderrHandler.addFilter(lambda x: x.levelno != logging.INFO)
    stderrHandler.setFormatter(logging.Formatter(
        '[%(levelname)s] %(name)s - %(message)s'))

    rootLogger.addHandler(stdoutHandler)
    rootLogger.addHandler(stderrHandler)

    run()
