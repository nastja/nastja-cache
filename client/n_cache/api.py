import logging
from importlib.metadata import version as pkg_version
from urllib.parse import quote
from uuid import UUID

import requests

from n_cache.nastja_config import NastjaConfig
from n_cache.simulation import Simulation

VERSION = pkg_version('n-cache')


class NCacheApi:
    def __init__(self, nastja_cache_url: str) -> None:
        self._base_url = nastja_cache_url
        self._first_request = True

    def _request(self, method, endpoint, params=None, **kwargs):
        r = requests.request(
            method, f'{self._base_url}{endpoint}', params=params, **kwargs)

        if self._first_request:
            server_version = r.headers.get('X-N-Cache-Version', 'NOT REPORTED')
            if server_version != VERSION:
                logging.warn(
                    f'Mismatch version server ({server_version}) / client ({VERSION}).')

            self._first_request = False

        return r

    def _get(self, endpoint, params=None, **kwargs):
        return self._request('get', endpoint, params, **kwargs)

    def _post(self, endpoint, data=None, json=None, **kwargs):
        return self._request('post', endpoint, data=data, json=json, **kwargs)

    def get_runner_groups(self):
        r = self._get('/api/v0/runner/groups')
        return r.json()

    def get_simulations(self, ids: list[str]):
        payload = {'id': ids}

        r = self._get('/api/v0/simulations', params=payload)
        return r.json()

    def post_simulations(self, simulations: list[Simulation]):
        payload = []

        for sim in simulations:
            elem = {
                'config': sim.config.get_config(),
                'frames': sim.frames
            }

            payload.append(elem)

        self.api.post_simulations()

        self._post('/api/v0/simulations', json=payload)

    def get_runner_group_config(self, group):
        group = quote(group)
        r = self._get(f'/api/v0/runner/groups/{group}/config')
        return r.json()

    def get_runner_job_batch(self, uuid: UUID, group):
        payload = {'id': uuid}
        group = quote(group)
        r = self._get(f'/api/v0/runner/groups/{group}/jobs', params=payload)
        r = r.json()
        r = list(map(lambda c: NastjaConfig(
            config=c['json'], id=c['id']), r['configs']))
        return r

    def post_runner_heartbeat(self, uuid: UUID):
        payload = {'guid': uuid}
        r = self._post('/api/v0/runner/heartbeat', json=payload)
        return r.json()

    def post_runner_group(self, runner_group):
        r = self._post('/api/v0/runner/groups', json=runner_group)
        return r.json()

    def post_runner_group_jobs(self, group, configs):
        payload = []

        for c in configs:
            payload.append(c.get_config())
        r = self._post(
            f'/api/v0/runner/groups/{quote(group)}/jobs', json=payload)
        return r.json()
