from abc import ABC, abstractmethod


class Command(ABC):
    @abstractmethod
    def run(args, nastja_cache_url: str) -> None:
        pass

    @abstractmethod
    def register(subparsers) -> None:
        pass
