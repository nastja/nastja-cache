import argparse
import logging
from email.policy import default

import requests

from n_cache.api import NCacheApi

from .command import Command


class AddGroupCommand(Command):
    def run(args, api: NCacheApi):
        runner_group = {
            'id': args.id,
            'description': args.description,
            'concurrent_sims': args.concurrent_sims,
            'nastja_command': args.nastja_command,
            'client_batch_size': args.client_batch_size,
            'server_batch_size': args.server_batch_size,
            'heartbeat_interval': args.heartbeat_interval
        }

        logging.info(f'Adding new runner group: {runner_group}')

        r = api.post_runner_group(runner_group)

        logging.info(f'Server response: {r}')

    def register(subparsers):
        subparser = subparsers.add_parser(
            'add-group', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        subparser.add_argument(
            '--id', help='ID (String) of runner group', required=True)
        subparser.add_argument(
            '--description', help='Some descriptive text. Just for you, human.', default='')
        subparser.add_argument(
            '--concurrent-sims', help='Amount of NAStJA instances a runner spawn locally in parallel.', default=16, type=int)
        subparser.add_argument(
            '--nastja-command', help='Command for invoking NAStJA', default='nastja')
        subparser.add_argument(
            '--client-batch-size', help='The runner will upload its results and get a new simulation batch after this count.', default=32, type=int)
        subparser.add_argument(
            '--server-batch-size', help='The server responds with (up to) this amount of simulations for the runner to simulate.', default=64, type=int)
        subparser.add_argument('--heartbeat-interval', help='When the runner requests simulation jobs, the returned configs are assigned to the requesting runner instance. '
                               + 'Another runner instance is not provided with configs assigned to another runner unless the assigned runner violates its herabeat interval. '
                               + 'In seconds.', default=10, type=int)

        subparser.set_defaults(run=AddGroupCommand.run)
