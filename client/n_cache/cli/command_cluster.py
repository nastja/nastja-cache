import argparse
import logging
from hashlib import new
from urllib.parse import quote

import requests

from n_cache.api import NCacheApi
from n_cache.config import Config
from n_cache.n_conf_generator import NastjaConfigGenerator
from n_cache.nastja_config import NastjaConfig

from .command import Command


class ClusterCommand(Command):
    def run(args, api: NCacheApi):
        config = Config(args.config, args.overrides, '', '')

        conf_gen = NastjaConfigGenerator(config.base_config, config.overrides)
        nastja_configs = conf_gen.gen_confs()

        new_count = ClusterCommand.schedule(api, nastja_configs, args.group)

        if new_count == 0:
            logging.info(
                'No new simulations scheduled. You can go ahead an download them from remote.')
        else:
            ClusterCommand.deploy_runner()

    def register(subparsers):
        subparser = subparsers.add_parser(
            'cluster', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        subparser.add_argument('group', help='Scheduling group')
        subparser.add_argument('config', help='Base config file')
        subparser.add_argument(
            'overrides', help='Description of alterations to base config')

        subparser.set_defaults(run=ClusterCommand.run)

    def schedule(api: NCacheApi, configs: list[NastjaConfig], group: str) -> int:

        logging.info('Sending job request...')
        r = api.post_runner_group_jobs(group, configs)

        new_count = r['new_count']
        logging.info(
            f'{new_count} simulations scheduled for cluster. {len(configs) - new_count} simulations allready available.')
        status_page = r['status_page']
        logging.info(f'You can view the job status at {status_page}.')

        return new_count

    def deploy_runner():
        logging.info('Deploy a runner to start job execution. Instructions are available at:\nhttps://gitlab.com/nastja/nastja-cache/-/wikis/Using-the-job-runner-with-Slurm-and-ENROOT')
