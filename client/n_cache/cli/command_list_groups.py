import logging
from email.policy import default
from pprint import pformat

import requests

from n_cache.api import NCacheApi

from .command import Command


class ListGroupsCommand(Command):
    def run(args, api: NCacheApi):
        r = api.get_runner_groups()

        logging.info(f'Server response:\n{pformat(r)}')

    def register(subparsers):
        subparser = subparsers.add_parser('list-groups')

        subparser.set_defaults(run=ListGroupsCommand.run)
