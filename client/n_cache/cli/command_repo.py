from n_cache.api import NCacheApi
from n_cache.config import Config
from n_cache.n_cache import NCache
from n_cache.provider.local_fs import LocalFSProvider

from .command import Command


class RepoCommand(Command):
    def run(args, api: NCacheApi):
        config = Config(args.config, args.overrides,
                        args.db_conn, api)
        n_cache = NCache(config)

        download = not args.skip_download
        upload = not args.skip_upload
        local = not args.skip_simulation

        local_fs = LocalFSProvider()
        for sim in n_cache.run(progress_bar=True, download=download, local=local, upload=upload):
            local_fs.store_simulations([sim])

    def register(subparsers):
        subparser = subparsers.add_parser('repo')
        default_db = 'sqlite+pysqlite:///local_repo.db'

        subparser.add_argument('config', help='Base config file')
        subparser.add_argument(
            'overrides', help='Description of alterations to base config')
        subparser.add_argument(
            '--db-conn', help=f'DB connection string. Defaults to "{default_db}"', default=default_db)

        subparser.add_argument(
            '--skip-download', help='Skip downloading simulations from global repository', action='store_true')
        subparser.add_argument(
            '--skip-upload', help='Skip uploading simulations to global repository', action='store_true')
        subparser.add_argument(
            '--skip-simulation', help='Do not execute NAStJA locally', action='store_true')

        subparser.set_defaults(run=RepoCommand.run)
