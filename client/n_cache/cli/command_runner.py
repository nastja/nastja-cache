import logging
import multiprocessing as mp
from tokenize import group
from urllib.parse import quote

import enlighten
import requests

from n_cache.api import NCacheApi
from n_cache.config import Config
from n_cache.nastja_config import NastjaConfig
from n_cache.provider.remote_rest import RemoteRestProvider
from n_cache.provider.threaded_local_nastja import ThreadedLocalNastjaProvider
from n_cache.runner.job_runner import JobRunner
from n_cache.runner.threaded_pipeline import ThreadedPipeline

from .command import Command


class RunnerCommand(Command):
    _group = ''
    _url = ''

    _batch_size = 1

    _awaiting = set()
    _in_queue = mp.Queue()
    _out_queue = mp.Queue()
    _bar = None

    def run(args, api: NCacheApi):
        runner = JobRunner(api, args.group)
        runner.run()

    def register(subparsers):
        subparser = subparsers.add_parser('runner')

        subparser.add_argument('group', help='Group ID')

        subparser.set_defaults(run=RunnerCommand.run)
