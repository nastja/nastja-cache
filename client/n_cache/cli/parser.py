import argparse
import os

from n_cache.api import NCacheApi
from n_cache.cli.command_add_group import AddGroupCommand
from n_cache.cli.command_cluster import ClusterCommand
from n_cache.cli.command_list_groups import ListGroupsCommand
from n_cache.cli.command_repo import RepoCommand
from n_cache.cli.command_runner import RunnerCommand
from n_cache.config import Config


def run():
    env_global_cache = os.environ.get(
        'NASTJA_CACHE_URL', 'http://n-cache.david-wilkening.de')
    default_db = 'sqlite+pysqlite:///local_repo.db'

    parser = argparse.ArgumentParser(
        description='Maintainer: David <kit@david-wilkening.de>', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--nastja-cache-url', help=f'Base URL of global cache. Defaults to NASTJA_CACHE_URL={env_global_cache}', default=env_global_cache)

    subparsers = parser.add_subparsers(required=True)

    RepoCommand.register(subparsers)
    ListGroupsCommand.register(subparsers)
    AddGroupCommand.register(subparsers)
    ClusterCommand.register(subparsers)
    RunnerCommand.register(subparsers)

    args = parser.parse_args()

    nastja_cache_url = args.nastja_cache_url.rstrip('/')
    # todo: redo config

    args.run(args, NCacheApi(nastja_cache_url))
