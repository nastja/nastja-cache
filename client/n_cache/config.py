import json
from dataclasses import dataclass
from typing import Union


@dataclass
class Config:
    base_config: dict
    overrides: dict
    db_conn_string: str
    base_url: str

    def __init__(self, base_config: Union[dict, str], overrides: Union[dict, str], db_conn_string: str, base_url: str) -> None:
        if type(base_config) == str:
            with open(base_config, 'r') as f_config:
                self.base_config = json.load(f_config)
        else:
            self.base_config = base_config
        if type(overrides) == str:
            with open(overrides, 'r') as f_overrides:
                self.overrides = json.load(f_overrides)['overrides']
        else:
            self.overrides = overrides
        self.db_conn_string = db_conn_string
        self.base_url = base_url
