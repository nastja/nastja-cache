import logging
import sys
from typing import Generator

import enlighten

from n_cache.api import NCacheApi

from .config import Config
from .n_conf_generator import NastjaConfigGenerator
from .provider.local_db import LocalDBProvider
from .provider.remote_rest import RemoteRestProvider
from .provider.threaded_local_nastja import ThreadedLocalNastjaProvider
from .simulation import Simulation


class NCache():
    def __init__(self, config: Config):
        self._config = config
        self._nastja_configs = {}
        conf_gen = NastjaConfigGenerator(config.base_config, config.overrides)
        for i in conf_gen.gen_confs():
            self._nastja_configs[i.get_id()] = i
        self._missing = set(self._nastja_configs.keys())

        self._from_local_db = set()
        self._from_local_fs = set()
        self._from_local_nastja = set()
        self._from_remote_db = set()

        self._local_db = LocalDBProvider(self._config)
        self._api = NCacheApi(self._config.base_url)
        self._remote_rest = RemoteRestProvider(self._api)
        self._local_nastja = ThreadedLocalNastjaProvider()

    def getLocalSimIds(self) -> set[str]:
        return self._from_local_nastja

    def run(self, progress_bar=False, download=True, local=False, upload=True) -> Generator[Simulation, None, None]:
        bar_read = enlighten.Counter(stream=(
            sys.stdout if progress_bar else None), total=len(self._missing), desc='Read')

        # Todo: defeats generators, replace with batch jobs
        sims_local_db = []
        sims_remote = []
        sims_local_nastja = []

        logging.info(
            f'Query {len(self._missing)} simulations from local db...')
        bar_r_local_db = bar_read.add_subcounter('blue')
        for sim in self._local_db.recv_simulations([self._nastja_configs[id] for id in self._missing]):
            bar_r_local_db.update()
            yield sim
            sims_local_db.append(sim)
            self._from_local_db.add(sim.config.get_id())

        logging.info(f'Loaded {len(self._from_local_db)} from local db.')
        self._missing.difference_update(self._from_local_db)

        if download:
            logging.info(
                f'Query {len(self._missing)} simulations from remote db...')
            bar_r_remote_db = bar_read.add_subcounter('green')
            for sim in self._remote_rest.recv_simulations([self._nastja_configs[id] for id in self._missing]):
                bar_r_remote_db.update()
                yield sim
                sims_remote.append(sim)
                self._from_remote_db.add(sim.config.get_id())

            logging.info(f'Loaded {len(self._from_remote_db)} from remote.')
            self._missing.difference_update(self._from_remote_db)

        if local:
            logging.info(
                f'Simulating {len(self._missing)} elements locally...')
            bar_r_local_sim = bar_read.add_subcounter('red')
            for sim in self._local_nastja.recv_simulations([self._nastja_configs[id] for id in self._missing]):
                bar_r_local_sim.update()
                yield sim
                sims_local_nastja.append(sim)
                self._from_local_nastja.add(sim.config.get_id())

            logging.info('Simulations done.')
            self._missing.difference_update(self._from_local_nastja)

        self._local_db.store_simulations(sims_remote)
        self._local_db.store_simulations(sims_local_nastja)
        logging.info('New simulations and remote query stored in local db.')
        if upload:
            logging.info('Uploading simulations to remote db...')
            self._remote_rest.store_simulations(sims_local_db)
            logging.info(f'Uploaded {len(sims_local_db)} from local db.')
            self._remote_rest.store_simulations(sims_local_nastja)
            logging.info(
                f'Uploaded {len(sims_local_nastja)} from local simulation.')
