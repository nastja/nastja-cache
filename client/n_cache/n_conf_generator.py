import logging

import jsonpointer

from .nastja_config import NastjaConfig


class NastjaConfigGenerator:
    def __init__(self, config, overrides):
        self._config = config
        self._overrides = overrides

    def gen_confs(self) -> list[NastjaConfig]:
        configset = self._gen_confs(self._config, self._overrides[0])
        configset = self._gen_confs_cross(configset)

        result = []
        for i in configset:
            result.append(NastjaConfig(i))

        logging.info(
            f'Calculated {len(result)} NAStJA configs from {len(self._overrides)} override rules.')

        return result

    def _gen_confs(self, base: dict, overwrite: dict) -> list:
        values = []
        if 'intervall' in overwrite:
            intervall = overwrite['intervall']
            step = overwrite.get('step', 1)
            values = range(intervall[0], intervall[1] + 1, step)
        elif 'enum' in overwrite:
            values = overwrite['enum']
        else:
            raise ValueError('unrecognized')

        result = []
        for val in values:
            result.append(jsonpointer.set_pointer(
                base, overwrite['path'], val, inplace=False))
        return result

    def _gen_confs_cross(self, configset: list) -> list:
        result = configset
        for override in self._overrides[1:]:
            intermediate = []
            for conf in result:
                intermediate.extend(self._gen_confs(conf, override))
            result = intermediate
        return result
