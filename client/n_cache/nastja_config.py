import hashlib
import json


class NastjaConfig:
    def __init__(self, config, id=None):
        self._config = config

        if id is None:
            as_json = json.dumps(config, separators=(',', ':'), sort_keys=True)
            m = hashlib.sha256()
            m.update(as_json.encode('utf-8'))
            self._id = m.hexdigest()
        else:
            self._id = id

    def get_id(self):
        return self._id

    def get_config(self):
        return self._config
