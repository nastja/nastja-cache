import logging

import numpy as np

from .config import Config
from .n_cache import NCache


class NumpyConsumer:
    def __init__(self, config: Config) -> None:
        self._config = config

    def getSims(self) -> np.ndarray:
        n_cache = NCache(self._config)

        np_res = []
        config_res = []
        for sim in n_cache.run(local=True, upload=False):
            np_sim = []
            for frame in sim.frames:
                lines = frame.split('\n')
                width = len(lines[0].split())
                np_frame = []
                for line in lines[1:]:
                    if line == '' or line.isspace():
                        continue
                    np_frame.append(np.fromstring(line, count=width, sep=' '))
                np_sim.append(np.stack(np_frame))
            np_res.append(np.stack(np_sim))
            config_res.append(sim)

        if n_cache.getLocalSimIds():
            logging.warn(
                "Simulated locally. Use 'n-cache repo' to cache for future usage.")
            logging.warn(f"Dumping IDs: \n{n_cache.getLocalSimIds()}")

        return np.stack(np_res), config_res
