import logging
from typing import Generator

from sqlalchemy import Column, Integer, String, create_engine, select
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

from n_cache.nastja_config import NastjaConfig

from ..config import Config
from ..simulation import Simulation
from .simulation_provider import SimulationProvider

Base = declarative_base()


class LocalDBProvider(SimulationProvider):
    def __init__(self, config: Config):
        self._engine = create_engine(config.db_conn_string, future=True)
        self._session = Session(self._engine)
        Base.metadata.create_all(self._engine)

    class DbSimulationFrame(Base):
        __tablename__ = 'simulations'

        id = Column(String, primary_key=True, autoincrement=False)
        frame = Column(Integer, primary_key=True, autoincrement=False)
        csv = Column(String)

    def _get_by_id(self, id: str) -> list[DbSimulationFrame]:
        stmt = select(self.DbSimulationFrame).where(
            self.DbSimulationFrame.id == id)
        res = self._session.execute(stmt).scalars().all()
        return res

    def recv_simulations(self, confs: list[NastjaConfig]) -> Generator[Simulation, None, None]:
        """
        Best effort receiver
        """
        for c in confs:
            sim = self._get_by_id(c.get_id())
            if len(sim) == 0:
                logging.debug(f'Simulation "{c.get_id()}" not in local db.')
                continue

            yield Simulation(c, [s.csv for s in sim])

    def store_simulations(self, simulations: list[Simulation]) -> None:
        for sim in simulations:
            for i, frame in enumerate(sim.frames, start=1):
                db_model = self.DbSimulationFrame(
                    id=sim.config.get_id(), frame=i, csv=frame)
                self._session.add(db_model)
        self._session.commit()
