import logging
from pathlib import Path
from typing import Generator

from ..nastja_config import NastjaConfig
from ..simulation import Simulation
from .simulation_provider import SimulationProvider


class LocalFSProvider(SimulationProvider):
    _target_dir = 'out/'

    def recv_simulations(self, confs: list[NastjaConfig]) -> Generator[Simulation, None, None]:
        # Todo: implement
        logging.error('Loading simulations from csv files not implemented.')

    def store_simulations(self, simulations: list[Simulation]) -> None:
        for sim in simulations:
            Path(
                f'{self._target_dir}{sim.config.get_id()}/').mkdir(parents=True, exist_ok=True)
            for i, frame in enumerate(sim.frames, start=1):
                f_path = f'{self._target_dir}{sim.config.get_id()}/output_cells-{i:05d}.csv'
                with open(f_path, 'w') as f:
                    f.write(frame)
