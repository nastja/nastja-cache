import fnmatch
import json
import logging
import os
import re
import subprocess
from tempfile import TemporaryDirectory
from typing import Generator

from ..nastja_config import NastjaConfig
from ..simulation import Simulation
from .simulation_provider import SimulationProvider


class LocalNastjaProvider(SimulationProvider):
    _target_dir = 'out/'
    _n_path = './nastja'

    def set_nastja_path(self, n_path: str):
        self._n_path = n_path

    def recv_simulations(self, confs: list[NastjaConfig]) -> Generator[Simulation, None, None]:
        for c in confs:
            with TemporaryDirectory() as tmpdir:
                c_path = f'{tmpdir}/conf.json'
                with open(c_path, 'w') as f_conf:
                    json.dump(c.get_config(), f_conf)

                try:
                    # NAStJA defaults to log errors to STDOUT.
                    subprocess.run(
                        [self._n_path, '-c', c_path, '-o', f'{tmpdir}/out'], check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                except subprocess.CalledProcessError as ex:
                    logging.error(
                        f'Could not run simulation for id "{c.get_id}". Exit code: {ex.returncode}. Dumping output:')
                    logging.error(ex.output)
                    continue

                files = os.listdir(f'{tmpdir}/out')
                csv_files = fnmatch.filter(
                    files, "output_cells-[0-9][0-9][0-9][0-9][0-9].csv")
                frames_filter = list(map(lambda x: (
                    int(re.search(
                        'output_cells-([0-9][0-9][0-9][0-9][0-9]).csv', x).group(1))
                ), csv_files))
                frames_filter.sort()

                frames = []
                if len(frames_filter) == 0:
                    logging.warning(
                        f'Simulation "{c.get_id()}" yielded no frames when simulated.')
                    logging.warning(c.get_config())
                for frame in frames_filter:
                    f_path = f'{tmpdir}/out/output_cells-{frame:05d}.csv'
                    with open(f_path, 'r') as f:
                        csv_content = f.read()
                        frames.append(csv_content)

                yield Simulation(c, frames)

    def store_simulations(self, simulations: list[Simulation]) -> None:
        """
        Noop. Can't store data inside the simulator.
        """
        simlist = "\n".join(map(lambda x: x.get_id(), simulations))
        logging.warn(
            f'Can not store simulations in local simulator (On ids "{simlist}", Computer is confused and hurt itself).')
