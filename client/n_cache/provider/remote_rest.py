from typing import Generator

import requests

from n_cache.api import NCacheApi

from ..nastja_config import NastjaConfig
from ..simulation import Simulation
from .simulation_provider import SimulationProvider


class RemoteRestProvider(SimulationProvider):
    def __init__(self, api: NCacheApi):
        self._api = api

    def recv_simulations(self, configs: list[NastjaConfig]) -> Generator[Simulation, None, None]:

        simulations = self._api.get_simulations(
            list([c.get_id() for c in configs]))

        current_id = None
        frames = []

        for frame in simulations:
            if current_id != None and frame['id'] != current_id:
                n_conf = next(
                    filter(lambda c: c.get_id() == current_id, configs))
                # Todo: handle None
                yield Simulation(n_conf, frames)

                frames = []
            # Todo: we depend on the server not betraying us
            current_id = frame['id']
            frames.append(frame['data'])

        if current_id != None:
            n_conf = next(filter(lambda c: c.get_id() == current_id, configs))
            # Todo: handle None
            yield Simulation(n_conf, frames)

    def store_simulations(self, simulations: list[Simulation]) -> None:
        self._api.post_simulations(simulations)
