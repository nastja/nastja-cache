from abc import ABC, abstractmethod
from typing import Generator

from ..nastja_config import NastjaConfig
from ..simulation import Simulation


class SimulationProvider(ABC):
    @abstractmethod
    def recv_simulations(self, confs: list[NastjaConfig]) -> Generator[Simulation, None, None]:
        """
        Best effort receiver
        """
        pass

    @abstractmethod
    def store_simulations(self, simulations: list[Simulation]) -> None:
        pass
