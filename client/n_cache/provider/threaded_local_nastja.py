import os
import queue
import threading
from typing import Generator

from n_cache.nastja_config import NastjaConfig
from n_cache.provider.local_nastja import LocalNastjaProvider
from n_cache.simulation import Simulation

NUM_THREADS = int(os.getenv('NUM_THREADS', '8'))


class ThreadedLocalNastjaProvider(LocalNastjaProvider):
    _in_queue = queue.Queue()
    _out_queue = queue.Queue()

    def recv_simulations(self, confs: list[NastjaConfig]) -> Generator[Simulation, None, None]:
        self._in_queue = queue.Queue()
        self._out_queue = queue.Queue()

        for c in confs:
            self._in_queue.put(c)

        worker_threads = []
        for i in range(NUM_THREADS):
            worker = threading.Thread(target=self._nastja_worker)
            worker.start()
            worker_threads.append(worker)

        for _ in range(len(confs)):
            res = self._out_queue.get()
            if res is not None:
                yield res

        for t in worker_threads:
            t.join()

    def _nastja_worker(self) -> None:
        while True:
            try:
                job = self._in_queue.get_nowait()
                res = next(super().recv_simulations([job]), None)
                self._out_queue.put(res)
            except queue.Empty:
                return

    def store_simulations(self, simulations: list[Simulation]) -> None:
        """
        Noop. Can't store data inside the simulator.
        """
        super().store_simulations(simulations)
