import logging
import multiprocessing as mp
from datetime import datetime
from pprint import pformat
from queue import Empty
from urllib.parse import quote
from uuid import uuid4

import enlighten
import requests

from n_cache.api import NCacheApi
from n_cache.config import Config
from n_cache.nastja_config import NastjaConfig
from n_cache.provider.remote_rest import RemoteRestProvider
from n_cache.runner.runner_config import RunnerConfig
from n_cache.runner.threaded_pipeline import ThreadedPipeline


class JobRunner():

    _url = ''
    _group = ''
    _uuid = uuid4()
    _config = None

    _awaiting = set()
    _in_queue = mp.Queue()
    _out_queue = mp.Queue()
    _bar = None

    def __init__(self, api: NCacheApi, group: str) -> None:
        self._api = api
        self._group = group
        group_conf = self._get_group_config()
        self._config = RunnerConfig.from_api(group_conf)

    def run(self):
        first_batch = self._get_job_batch()

        if len(first_batch) == 0:
            logging.info('Job queue empty. Nothing to do. (Correct group?).')
            return

        worker = mp.Process(target=JobRunner._worker, args=(
            self._in_queue, self._out_queue, self._config))

        for c in first_batch:
            self._awaiting.add(c.get_id())
            self._in_queue.put(c)

        self._bar = enlighten.Counter(total=len(first_batch))

        logging.info(
            'Starting simulation. ETA will be calculated after first result.')
        worker.start()
        self._orchestration_loop()
        worker.join()
        logging.info('jobs complete.')

    def _get_group_config(self):
        logging.info('Query group config.')
        r = self._api.get_runner_group_config(self._group)
        logging.info(pformat(r))
        return r

    def _get_job_batch(self):
        return self._api.get_runner_job_batch(self._uuid, self._group)

    def _upload(self, configs):
        remote_repo = RemoteRestProvider(Config({}, {}, '', self._url))
        remote_repo.store_simulations(configs)

    def _do_heartbeat(self):
        self._api.post_runner_heartbeat(self._uuid)

    def _orchestration_loop(self):
        out_buffer = []
        timeout = float(self._config.heartbeat_interval)
        last_heartbeat = datetime.utcnow()
        while True:
            try:
                latest = self._out_queue.get(
                    timeout - (datetime.utcnow() - last_heartbeat).total_seconds())
                if (datetime.utcnow() - last_heartbeat).total_seconds() >= timeout:
                    self._do_heartbeat()
                    last_heartbeat = datetime.utcnow()

                if latest:
                    self._bar.update()
                    out_buffer.append(latest)
                    self._awaiting.remove(latest.config.get_id())
                else:
                    break

                if len(out_buffer) >= self._config.client_batch_size:
                    self._upload(out_buffer)
                    out_buffer = []
                    new_batch = self._get_job_batch()
                    if new_batch:
                        for c in new_batch:
                            if c.get_id() in self._awaiting:
                                continue
                            self._awaiting.add(c.get_id())
                            self._in_queue.put(c)
                            self._bar.count += 1
                    else:
                        self._in_queue.put(None)
                        return
            except Empty:
                self._do_heartbeat()
                last_heartbeat = datetime.utcnow()

        if out_buffer:
            logging.info('Uploading last batch...')
            self._upload(out_buffer)

    def _worker(in_queue: mp.Queue, out_queue: mp.Queue, config: RunnerConfig):
        pipe_worker = ThreadedPipeline(
            in_queue, out_queue, config.nastja_command)
        pipe_worker.run(config.concurrent_sims)
