from dataclasses import dataclass
from urllib.parse import quote


@dataclass
class RunnerConfig:
    description: str
    concurrent_sims: int
    nastja_command: str
    client_batch_size: int
    server_batch_size: int
    heartbeat_interval: int

    def from_api(json: dict):
        return RunnerConfig(json['description'], json['concurrent_sims'], json['nastja_command'], json['client_batch_size'], json['server_batch_size'], json['heartbeat_interval'])
