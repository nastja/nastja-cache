import multiprocessing as mp
import threading
from queue import Queue as ThreadQueue

from n_cache.provider.local_nastja import LocalNastjaProvider


class ThreadedPipeline():
    _in_queue = None
    _out_queue = None
    _internal_queue = ThreadQueue()
    _sim_provider = LocalNastjaProvider()

    def __init__(self, in_queue: mp.Queue, out_queue: mp.Queue, nastja_path: str) -> None:
        self._in_queue = in_queue
        self._out_queue = out_queue
        self._sim_provider.set_nastja_path(nastja_path)

    def run(self, thread_count: int):
        worker_threads = []
        for i in range(thread_count):
            worker = threading.Thread(target=self._nastja_worker)
            worker.start()
            worker_threads.append(worker)

        while True:
            latest = self._in_queue.get()
            if latest:
                self._internal_queue.put(latest)
            else:
                for _ in range(thread_count):
                    self._internal_queue.put(None)
                break

        for t in worker_threads:
            t.join()

    def _nastja_worker(self) -> None:
        while True:
            job = self._internal_queue.get()
            if job:
                res = next(self._sim_provider.recv_simulations([job]), None)
                self._out_queue.put(res)
            else:
                return
