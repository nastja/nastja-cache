from dataclasses import dataclass

from .nastja_config import NastjaConfig


@dataclass
class Simulation:
    config: NastjaConfig
    frames: list[str]
