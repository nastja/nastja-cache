N-Cache Server
==============

# Installation

## Development

Install the project as an [editable install][edit-install] to make easy changes.

- Clone project.
- Navigate to `server/` subdir.
- Execute the following:

```bash
$ pip install -e .
docker-compose --env-file .env.dev -f docker-compose.dev.yml -d up
# wait for postgres to start
alembic upgrade head
source read_env.sh
n-cache-server # start server in debug mode with hot reload
```

## Deployment

Run `deploy.sh` after cloning repo.


[edit-install]: https://pip.pypa.io/en/stable/cli/pip_install/#editable-installs