"""init

Revision ID: 0ee92dd8be40
Revises: 
Create Date: 2021-11-21 09:33:59.512778

"""
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.sql.schema import ForeignKey

from alembic import op

# revision identifiers, used by Alembic.
revision = '0ee92dd8be40'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # Postgresql supports JSONB to store json more efficiently
    bind = op.get_bind()
    if bind.engine.name == 'postgresql':
        op.create_table(
            'sim_configs',
            sa.Column('id', sa.String, primary_key=True),
            sa.Column('json', JSONB),
        )
    else:
        op.create_table(
            'sim_configs',
            sa.Column('id', sa.String, primary_key=True),
            sa.Column('json', sa.JSON),
        )

    op.create_table(
        'simulations',
        sa.Column('id', sa.String, ForeignKey(
            'sim_configs.id'), primary_key=True),
        sa.Column('frame', sa.Integer, primary_key=True),
        sa.Column('data', sa.String),
    )

    op.create_table(
        'cluster_jobs',
        sa.Column('id', sa.Integer, sa.Identity(), primary_key=True),
        sa.Column('created', sa.DateTime),
        sa.Column('finished', sa.DateTime),
    )

    op.create_table(
        'jobs_config',
        sa.Column('job_id', sa.Integer, ForeignKey(
            'cluster_jobs.id'), primary_key=True),
        sa.Column('config_id', sa.String, ForeignKey(
            'sim_configs.id'), primary_key=True),
        sa.Column('is_finished', sa.Boolean),
    )


def downgrade():
    pass
