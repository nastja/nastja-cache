"""Add runner groups

Revision ID: 989a5ea17927
Revises: 0ee92dd8be40
Create Date: 2022-02-07 09:47:55.466527

"""
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

from alembic import op

# revision identifiers, used by Alembic.
revision = '989a5ea17927'
down_revision = '0ee92dd8be40'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'runner_instances',
        sa.Column('id', UUID, primary_key=True),
        sa.Column('last_ping', sa.DateTime),
    )

    op.create_table(
        'runner_groups',
        sa.Column('id', sa.String, primary_key=True),
        sa.Column('description', sa.String),
        sa.Column('concurrent_sims', sa.Integer),
        sa.Column('nastja_command', sa.String),
        sa.Column('client_batch_size', sa.Integer),
        sa.Column('server_batch_size', sa.Integer),
        sa.Column('heartbeat_interval', sa.Integer),
    )

    op.execute("DELETE FROM jobs_config;")
    op.execute("DELETE FROM cluster_jobs;")

    # todo: migrate to default group
    op.add_column('cluster_jobs', sa.Column(
        'group_id', sa.String, sa.ForeignKey('runner_groups.id')))

    op.add_column('jobs_config', sa.Column(
        'runner_id', UUID, sa.ForeignKey('runner_instances.id')))


def downgrade():
    pass
