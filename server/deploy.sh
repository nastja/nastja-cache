#!/bin/bash

echo 'Run this script inside the server/ subdir or funky things might happen!'

echo 'DB Username?'
read db_user
echo 'DB Password?'
read -s db_pass
echo 'DB mount location?'
read -e db_host_mnt
if ! [[ $db_host_mnt = /* ]]
then
    db_host_mnt="$(pwd)/$db_host_mnt"
fi

echo 'pgadmin user mail?'
read pgadmin_mail
echo 'pgadmin password?'
read -s pgadmin_pass

echo 'Slack webhook URL?'
read slack_hook

cat <<EOF >.env.prod
DB_USER=$db_user
DB_PASS=$db_pass
DB_NAME=nastja-cache
DB_HOST_MNT=$db_host_mnt
PGADMIN_MAIL=$pgadmin_mail
PGADMIN_PASS=$pgadmin_pass
SLACK_HOOK_URL=$slack_hook
EOF

docker-compose --env-file .env.prod -f docker-compose.prod.yml up -d
docker logs postgres -f | tee >(grep -q 'database system is ready to accept connections')
echo '\n\n'
echo 'Postgres up according to log. Wating 2 seconds before running db migrations.'
sleep 2
docker exec -it n-cache alembic upgrade head
