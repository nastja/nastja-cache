import os
from urllib.parse import quote

from flask import Flask

from n_cache_server.api_v0_runner import bp_runner
from n_cache_server.api_v0_simulations import bp_sims
from n_cache_server.db import db
from n_cache_server.version_header import append_version_header
from n_cache_server.web import bp_web


def create_app(db_uri):
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
    db.init_app(app)

    app.register_blueprint(bp_web)
    app.register_blueprint(bp_sims, url_prefix='/api/v0/simulations')
    app.register_blueprint(bp_runner, url_prefix='/api/v0/runner')

    app.after_request(append_version_header)

    return app


def app_by_env():
    DB_HOST = os.getenv('DB_HOST')
    DB_USER = os.getenv('DB_USER')
    DB_PASS = os.getenv('DB_PASS')
    DB_NAME = os.getenv('DB_NAME')

    return create_app(
        f'postgresql://{quote(DB_USER)}:{quote(DB_PASS)}@{quote(DB_HOST)}/{quote(DB_NAME)}')


def main():
    app = app_by_env()
    app.run(host='0.0.0.0', port=8000, debug=True)
