import hashlib
import json
from datetime import datetime, timedelta

from flask import Blueprint, jsonify, make_response, request

from n_cache_server.db import (JobConfig, RunnerInstance, SimulationConfig,
                               SimulationFrame, SimulationJob, db)

bp_sims = Blueprint('v0_simulations', __name__)


def config_to_id(config: dict) -> str:
    as_json = json.dumps(config, separators=(',', ':'), sort_keys=True)
    m = hashlib.sha256()
    m.update(as_json.encode('utf-8'))
    return m.hexdigest()


@bp_sims.route("", methods=['POST'])
def post_simulations():
    json_data = request.get_json()
    finished_jobs = []
    for sim in json_data:
        sim_id = config_to_id(sim['config'])

        if db.session.get(SimulationConfig, sim_id) is None:
            db_sim_conf = SimulationConfig(id=sim_id, json=sim['config'])
            db.session.add(db_sim_conf)

        SimulationFrame.query.filter(SimulationFrame.id == sim_id).delete()

        for i, frame_data in enumerate(sim['frames'], start=1):
            db_frame = SimulationFrame(id=sim_id, frame=i, data=frame_data)
            db.session.add(db_frame)

        job_configs = JobConfig.query.filter(
            JobConfig.config_id == sim_id).all()
        for jc in job_configs:
            jc.is_finished = True
            job = SimulationJob.query.get(jc.job_id)
            job_done = JobConfig.query.filter(
                JobConfig.job_id == job.id, JobConfig.is_finished == False).first() is None
            if job_done:
                job.finished = datetime.utcnow()
                finished_jobs.append(job.id)

        db.session.flush()

    # todo: remove unassigned runners
    # if finished_jobs:
    #    RunnerInstance.query.join(JobConfig).filter(JobConfig.is_finished == True).all()

    db.session.commit()

    return make_response(('', 201))


@bp_sims.route("", methods=['GET'])
def get_simulations():
    sim_ids = request.args.getlist('id')
    res = db.session.query(SimulationFrame).filter(
        SimulationFrame.id.in_(sim_ids)).all()
    res = [r.to_dict() for r in res]
    return jsonify(res)
