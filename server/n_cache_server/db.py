from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSONB, UUID

db = SQLAlchemy(engine_options={'isolation_level': 'REPEATABLE READ'})


class SimulationFrame(db.Model):
    __tablename__ = 'simulations'
    id = db.Column(db.String, db.ForeignKey(
        'sim_configs.id'), primary_key=True)
    frame = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String)

    def to_dict(self):
        return {
            'id': self.id,
            'frame': self.frame,
            'data': self.data
        }


class SimulationConfig(db.Model):
    __tablename__ = 'sim_configs'
    id = db.Column(db.String, primary_key=True)
    json = db.Column(JSONB)

    def to_dict(self):
        return {
            'id': self.id,
            'json': self.json
        }


class SimulationJob(db.Model):
    __tablename__ = 'cluster_jobs'
    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.String, db.ForeignKey('runner_groups.id'))
    created = db.Column(db.DateTime)
    finished = db.Column(db.DateTime)

    def to_dict(self):
        return {
            'id': self.id,
            'group_id': self.group_id,
            'created': self.created,
            'finished': self.finished
        }


class JobConfig(db.Model):
    __tablename__ = 'jobs_config'
    job_id = db.Column(db.Integer, db.ForeignKey(
        'cluster_jobs.id'), primary_key=True)
    config_id = db.Column(db.String, db.ForeignKey(
        'sim_configs.id'), primary_key=True)
    runner_id = db.Column(UUID, db.ForeignKey(
        'runner_instances.id'))
    is_finished = db.Column(db.Boolean)


class RunnerInstance(db.Model):
    __tablename__ = 'runner_instances'
    id = db.Column(UUID, primary_key=True)
    last_ping = db.Column(db.DateTime)


class RunnerGroup(db.Model):
    __tablename__ = 'runner_groups'
    id = db.Column(db.String, primary_key=True)
    description = db.Column(db.String)
    concurrent_sims = db.Column(db.Integer)
    nastja_command = db.Column(db.String)
    client_batch_size = db.Column(db.Integer)
    server_batch_size = db.Column(db.Integer)
    heartbeat_interval = db.Column(db.Integer)

    def to_dict(self):
        return {
            'id': self.id,
            'description': self.description,
            'concurrent_sims': self.concurrent_sims,
            'nastja_command': self.nastja_command,
            'client_batch_size': self.client_batch_size,
            'server_batch_size': self.server_batch_size,
            'heartbeat_interval': self.heartbeat_interval,
        }
