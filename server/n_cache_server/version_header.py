from functools import wraps
from importlib.metadata import version as pkg_version

from flask import Response

VERSION = pkg_version('n_cache_server')


def append_version_header(response: Response):
    response.headers['X-N-Cache-Version'] = VERSION
    return response
