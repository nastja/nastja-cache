import json
from datetime import datetime, timedelta

from flask import Blueprint, render_template

from n_cache_server.db import JobConfig, SimulationConfig, SimulationJob, db

bp_web = Blueprint('web', __name__)


@bp_web.route("/")
@bp_web.route("/index.html")
def index():
    return render_template('index.html')


@bp_web.route("/jobs")
def jobs():
    in_progress = db.session.query(SimulationJob).filter(
        SimulationJob.finished == None).all()
    threshold = datetime.utcnow() - timedelta(days=3)
    recent = db.session.query(SimulationJob).filter(
        SimulationJob.finished != None, SimulationJob.finished >= threshold).all()
    return render_template('jobs.html', in_progress=in_progress, recent=recent)


@bp_web.route("/jobs/<job_id>")
def jobs_detail(job_id: int):
    job = SimulationJob.query.get_or_404(job_id)
    sims = JobConfig.query.filter(JobConfig.job_id == job_id).all()
    remaining = filter(lambda s: not s.is_finished, sims)
    available = filter(lambda s: s.is_finished, sims)
    return render_template('jobs_detail.html', job=job, remaining=remaining, available=available)


@bp_web.route("/simulations/<sim_id>")
def simulations_detail(sim_id: int):
    sim = SimulationConfig.query.get_or_404(sim_id)
    config_pretty = json.dumps(sim.json, indent=2)
    print(config_pretty)
    return render_template('simulations_detail.html', simulation=sim, config_pretty=config_pretty)
