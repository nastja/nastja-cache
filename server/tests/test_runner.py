import datetime
import hashlib
import json
import unittest
from unittest.mock import Mock, patch
from uuid import uuid4

import n_cache_server
import n_cache_server.__main__ as n_cache
from n_cache_server import api_v0_runner
from n_cache_server.db import (JobConfig, RunnerGroup, SimulationConfig,
                               SimulationFrame, SimulationJob, db)


def config_to_id(config: dict) -> str:
    as_json = json.dumps(config, separators=(',', ':'), sort_keys=True)
    m = hashlib.sha256()
    m.update(as_json.encode('utf-8'))
    return m.hexdigest()


class RunnerTestCase(unittest.TestCase):
    def setUp(self):
        self.app = n_cache.create_app('postgresql://dev:dev@localhost/dev')
        self.app.app_context().push()
        self.app.testing = True
        self.app.debug = False
        #self.app.config['SQLALCHEMY_ECHO'] = True
        db.drop_all()
        db.create_all()
        self.client = self.app.test_client()

    def tearDown(self):
        db.session.commit()

    def test_get_groups(self):
        assert self.client.get('/api/v0/runner/groups').json['groups'] == {}

        new_group = RunnerGroup(id='test_group',
                                description='test runner group',
                                concurrent_sims=8,
                                nastja_command='./nastja',
                                client_batch_size=32,
                                server_batch_size=64,
                                heartbeat_interval=10)

        db.session.add(new_group)
        db.session.commit()

        assert self.client.get('/api/v0/runner/groups').json['groups'] == \
            {'test_group': {'client_batch_size': 32,
                            'concurrent_sims': 8,
                            'description': 'test runner group',
                            'heartbeat_interval': 10,
                            'id': 'test_group',
                            'nastja_command': './nastja',
                            'server_batch_size': 64}}

    def test_add_group(self):
        assert RunnerGroup.query.all() == []

        payload = {
            'id': 'test_group',
            'description': 'test runner group',
            'concurrent_sims': 8,
            'nastja_command': './nastja',
            'client_batch_size': 32,
            'server_batch_size': 64,
            'heartbeat_interval': 10
        }
        self.client.post('/api/v0/runner/groups', json=payload)

        res = RunnerGroup.query.all()
        assert len(res) == 1
        res = res[0]
        assert res.id == 'test_group'
        assert res.description == 'test runner group'
        assert res.concurrent_sims == 8
        assert res.nastja_command == './nastja'
        assert res.client_batch_size == 32
        assert res.server_batch_size == 64
        assert res.heartbeat_interval == 10

    def test_add_jobs(self):
        new_group = RunnerGroup(id='test_group',
                                description='test runner group',
                                concurrent_sims=8,
                                nastja_command='./nastja',
                                client_batch_size=32,
                                server_batch_size=64,
                                heartbeat_interval=10)

        db.session.add(new_group)
        db.session.commit()

        c1_c = {'config': 1}
        c1_id = config_to_id(c1_c)
        c2_c = {'config': 2}
        c2_id = config_to_id(c2_c)

        db_c1 = SimulationConfig(id=c1_id, json=c1_c)
        db.session.add(db_c1)
        db_f1 = SimulationFrame(id=c1_id, frame=0, data='foo')
        db.session.add(db_f1)
        db.session.commit()

        payload = [c1_c, c2_c]
        res = self.client.post(
            '/api/v0/runner/groups/test_group/jobs', json=payload).json

        assert res['new_count'] == 1
        assert res['status_page']

        jobs = JobConfig.query.all()
        assert len(jobs) == 2

        for j in jobs:
            if j.config_id == c1_id:
                assert j.is_finished == True
            else:
                assert j.config_id == c2_id
                assert j.is_finished == False

        job_grouped = SimulationJob.query.all()
        assert len(job_grouped) == 1
        job_grouped = job_grouped[0]

        assert job_grouped.group_id == 'test_group'
        assert job_grouped.finished == None

    @patch.object(api_v0_runner, 'datetime', Mock(wraps=datetime.datetime))
    def test_get_job_batch_simple(self):
        new_group = RunnerGroup(id='test_group',
                                description='test runner group',
                                concurrent_sims=8,
                                nastja_command='./nastja',
                                client_batch_size=1,
                                server_batch_size=1,
                                heartbeat_interval=10)

        db.session.add(new_group)
        db.session.commit()

        c1_c = {'config': 1}
        c2_c = {'config': 2}

        payload = [c1_c, c2_c]
        self.client.post('/api/v0/runner/groups/test_group/jobs', json=payload)

        runner_id = uuid4()

        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 0)
        res = self.client.get('/api/v0/runner/groups/test_group/jobs',
                              query_string={'id': str(runner_id)}).json

        assert len(res['configs']) == 1
        first_id = res['configs'][0]['id']
        first_config = res['configs'][0]['json']

        self.client.post('/api/v0/simulations/',
                         json=[{'config': first_config, 'frames': ['']}])

        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 1)
        res = self.client.get('/api/v0/runner/groups/test_group/jobs',
                              query_string={'id': str(runner_id)}).json

        assert len(res['configs']) == 1
        assert first_id != res['configs'][0]['id']

        self.client.post('/api/v0/simulations/',
                         json=[{'config': res['configs'][0]['json'], 'frames': ['']}])

        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 2)
        res = self.client.get('/api/v0/runner/groups/test_group/jobs',
                              query_string={'id': str(runner_id)}).json

        assert len(res['configs']) == 0

    @patch.object(api_v0_runner, 'datetime', Mock(wraps=datetime.datetime))
    def test_get_job_batch_multiple(self):
        new_group = RunnerGroup(id='test_group',
                                description='test runner group',
                                concurrent_sims=8,
                                nastja_command='./nastja',
                                client_batch_size=1,
                                server_batch_size=1,
                                heartbeat_interval=10)

        db.session.add(new_group)
        db.session.commit()

        c1_c = {'config': 1}
        c2_c = {'config': 2}

        payload = [c1_c, c2_c]
        self.client.post('/api/v0/runner/groups/test_group/jobs', json=payload)

        runner_first_id = uuid4()
        runner_second_id = uuid4()

        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 0)
        res_a = self.client.get('/api/v0/runner/groups/test_group/jobs',
                                query_string={'id': str(runner_first_id)}).json
        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 1)
        res_b = self.client.get('/api/v0/runner/groups/test_group/jobs',
                                query_string={'id': str(runner_second_id)}).json

        assert res_a['configs'][0]['id'] != res_b['configs'][0]['id']

        # when not finished return allready assigned simulation
        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 2)
        res_aa = self.client.get(
            '/api/v0/runner/groups/test_group/jobs', query_string={'id': str(runner_first_id)}).json
        assert res_a['configs'][0]['id'] == res_aa['configs'][0]['id']

        self.client.post('/api/v0/simulations/', json=[
            {'config': c1_c, 'frames': ['']},
            {'config': c2_c, 'frames': ['']}])

        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 3)
        res = self.client.get('/api/v0/runner/groups/test_group/jobs',
                              query_string={'id': str(runner_first_id)}).json

        assert len(res['configs']) == 0

    @patch.object(api_v0_runner, 'datetime', Mock(wraps=datetime.datetime))
    def test_get_job_batch_timeout(self):
        new_group = RunnerGroup(id='test_group',
                                description='test runner group',
                                concurrent_sims=8,
                                nastja_command='./nastja',
                                client_batch_size=1,
                                server_batch_size=1,
                                heartbeat_interval=10)

        db.session.add(new_group)
        db.session.commit()

        c1_c = {'config': 1}

        payload = [c1_c]
        self.client.post('/api/v0/runner/groups/test_group/jobs', json=payload)

        runner_first_id = uuid4()
        runner_second_id = uuid4()

        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 0)
        res_a = self.client.get('/api/v0/runner/groups/test_group/jobs',
                                query_string={'id': str(runner_first_id)}).json
        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 1)
        res_b = self.client.get('/api/v0/runner/groups/test_group/jobs',
                                query_string={'id': str(runner_second_id)}).json

        assert len(res_a['configs']) == 1
        assert len(res_b['configs']) == 0

        # after timeout, reassign job to new runner
        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 22)
        res_bb = self.client.get('/api/v0/runner/groups/test_group/jobs',
                                 query_string={'id': str(runner_second_id)}).json
        api_v0_runner.datetime.utcnow.return_value = datetime.datetime(
            2022, 1, 1, 1, 0, 23)
        res_aa = self.client.get(
            '/api/v0/runner/groups/test_group/jobs', query_string={'id': str(runner_first_id)}).json

        assert len(res_aa['configs']) == 0
        assert len(res_bb['configs']) == 1
